AJS.toInit(function () {
    var pageState = require("model/page-state")
    var url = AJS.contextPath() + "/rest/branchForCommit/1.0/BranchDetails" 
    	+ "/projects/" + pageState.getProject().getKey()
    	+ "/repos/" + pageState.getRepository().getSlug()
    	+ "/commits/" + pageState.getChangeset().getId() + "/value";
    AJS.$.ajax({
        type: 'GET',
        url: url,
        success: function(data) {
       	 	AJS.$("<div class='branchDetails'></div>").insertAfter('.changeset-metadata div.changeset-metadata-changeset-id');
               var branches = data.result.split( ', ' );
               var result = '';
               for( var i = 0; i < branches.length; i++  ) {
                   result += '<a href="' +  data.baseUrl + '?until=refs/heads/' + branches[i] + '">' + branches[i] + '</a>';
                   if( i < branches.length-1 ) {
                       result += ', ';
                   }
               }
               AJS.$('.branchDetails').html( result );
        },
        error: function() {
            console.error("Error GETing branches from stash");
        },
        contentType: "application/json"
    });
});
