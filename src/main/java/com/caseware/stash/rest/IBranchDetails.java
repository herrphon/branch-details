package com.caseware.stash.rest;

import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import com.atlassian.stash.repository.Repository;

public interface IBranchDetails {
	public Response getBranchForCommit(@Context final Repository repository, @PathParam("guid") final String guid);
}
